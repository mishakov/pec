Objectivo: recolectar energia solar

Crear un sistem capaz de :

- identificar el usuario que accede al recinto
- abrir y cerrar la puerta de acceso
- crear un log temporal de accesos y movimientos de los linces --> RTC
- recinto cerrado
- balance energetico de la placa

Con ICs dedicados para cada funcion


COMUNICACION:
-bluetooth ---> centralizacion de los dispositivos para asi no superar el rango que permite esta tecnologia
-establir comunicacio entre movil i estacio amb bluetooth i el dispositiu el que fa es un "dump" de informacio al telefon i el telefon genera el LOG a partir d'una aplicacio (BONA IDEA)

TIEMPO:
-intentar evitar el uso de GPS porque es muy caro

BATERIA:
- valorar quin percentatge de temps tindrem acces a llum solar depenent de la posicio del recinte --> guardar consum d'energia tambe en timestamp per veure que la bateria esta ben pensada per la quantitat de llum que es capac d'emmagatzemar en aquest clima

TEMPERATURA:
- timestamp + temperatura amb RFC anem creant timestamps quan detectem pics de temperatura (BONA IDEA)

Webs per components:
FARNELL
RS-AMIDATA
DIGIKEY
MOUSER

ESTACION BEBEDERO/COMEDERO
- LCD SCREEN PARA DAR FEEDBACK DE CONEXION ESTABLECIDA CON EL MOVIL
- BOTON FISICO CAPACITATIVO PARA ENLAZAR BLUETOOTH
- TECLADO AUXILIAR PARA INTRODUCIR CODIGO DE OPERARIO PARA QUE POR PANTALLA SE VEAN LOS DATOS I SE PUEDAN CREAR MANUALMENTE LOS LOGS EN LA APLICACION
- PLACA SOLAR (https://voltaicsystems.com/1-watt-panel/) (https://voltaicsystems.com/W045)
- BATERIA
- SENSOR DE MOVIMIENTO PARA DETECTAR PRESENCIA DE LINCES
- CONTROLADOR DE CARGA DE BATERIA
- RFC

ESTACION ENTRADA RECINTO:
- RFC 
- TERMOMETRO
- MODULO RFID
- MECANISMO DE ACTIVACION/DESACTIVACION PUERTA

PRESSUPOST: 200€


