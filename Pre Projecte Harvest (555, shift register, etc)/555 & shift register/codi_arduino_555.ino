const int buttonInPin = A3;
const int s0Pin = A4;
const int s1Pin = A5;

const int srSelPin = 1;
const int slSelPin = 0;

const int btn2InPin = 13;

void flipMode() {
  static bool mode = false;
  mode = !mode;

  if (mode) {
    digitalWrite(s0Pin,  HIGH);
    digitalWrite(s1Pin,  LOW);
  } else {
    digitalWrite(s0Pin,  LOW);
    digitalWrite(s1Pin,  HIGH);
  }

  Serial.print("Flipped mode to: ");
  Serial.println(mode);
}

void setup() {
  pinMode(buttonInPin, INPUT);
  pinMode(btn2InPin, INPUT);
  pinMode(s0Pin, OUTPUT);
  pinMode(s1Pin, OUTPUT);
  pinMode(srSelPin, OUTPUT);
  pinMode(slSelPin, OUTPUT);
}

void bounceBtn() {
  int btn = digitalRead(btn2InPin);
  digitalWrite(srSelPin, btn);
  digitalWrite(slSelPin, btn);
}

void loop() {
  while (digitalRead(buttonInPin)) {bounceBtn();}

  flipMode();

  delay(20);

  while (!digitalRead(buttonInPin)) {bounceBtn();}

  delay(20);

  
}
