## Projecte PEC
Aquest repositori conté el projecte, desenvolupat per Mikhail Kovalev i Bernat Homs, per l'assignatura [PEC](https://www.fib.upc.edu/ca/estudis/graus/grau-en-enginyeria-informatica/pla-destudis/assignatures/PEC), que constava de crear un sistema informàtic integrat basat en microcontroladors, alimentats amb energia solar i comunicació sense fils, fent servir Bluetooth i WiFi.
# Objectius i components
L'objectiu final d’aquest treball era desenvolupar un sistema informàtic per un escenari imaginari de l'assignatura, que incloïa fer seguiment d’animals i també un sistema de seguretat i control d'accés dels treballadors. Originalment, el sistema tenia els següents requisits:
- Un sistema de control d’accés de treballadors a un perímetre tancat
- Una eina per controlar els moviments d’animals i també alguns paràmetres ambientals, com la temperatura i humitat.
- Tots els components de sistema havien de tenir baix consum d’energia i alimentació amb energia solar
- Els mòduls havien de tindre comunicació sense fills.
El sistema final incloïa els següents components:
- Dos microcontroladors, ESP32 dins del mòdul central i WBZ451 dins del mòdul satèl·lit.
- Lector RFID i motor, controlats amb ESP32 i H-Bridge, per fer el control d’accés
- Sensor PIR i de temperatura, dins del mòdul satèl·lit, per fer seguiment dels animals
- Un mòdul AEM10941, placa solar i bateria, per alimentar els microcontroladors
- Bluetooth LE per fer comunicació entre microcontroladors
- WiFi, amb l’ús de Websocket i JSON, acoblat amb una aplicació web, per enviar esdeveniments i controlar els microcontroladors amb un servidor núvol. 
- PCB i caixa feta amb una impressora 3D, dissenyades per nosaltres
# Estructura i contingut del projecte
Aquest projecte està estructurat de la següent manera:
- Apunts: alguns apunts de l’assignatura i planificació inicial
- Pre projecte harvest: algunes coses que hem fet abans del projecte final, com soldadura i interacció amb 555 i registre shift
- Projecte Harvest: tot relacionat amb el projecte final, com els materials, codis dels microcontroladors i web, esquemàtics i models 3D, presentacions del projecte
